### **1.** First of all you need to clone this repository
```python3
git clone https://gitlab.com/karynabarkhatova/hometask2.git
```
### **2.** Now you need to install a virtual environment
```python3
python3 -m venv virtual_env
```
### **3.** Then you need to activate your virtual environment
```python3
source virtual_env/bin/activate
```
### **4.** Install the pack of files from requirements.txt
```python3
pip install -r requirements.txt
```
### Now you can use flake8 to fix problems if they are 
```python3
flake8 main.py
```
### If there're no problems you can run a program 
```python3
python3 main.py
``` 
